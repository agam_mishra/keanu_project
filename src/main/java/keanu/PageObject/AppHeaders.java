package keanu.PageObject;

import java.io.IOException;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import keanu.Library.AppLibrary;

public class AppHeaders {

	private AppLibrary appLibrary;
	AppiumDriver<MobileElement> driver;
	public static String chatScreen = "xpath://*[contains(@class,'android.support.v7.app')][@index='0']";
	public static String friendScreen = "xpath://*[contains(@class,'android.support.v7.app')][@index='1']";
	public static String discoverScreen = "xpath://*[contains(@class,'android.support.v7.app')][@index='2']";
	public static String myProfileScreen = "xpath://*[contains(@class,'android.support.v7.app')][@index='3']";
	public static String searchHistory = "id:info.guardianproject.keanuapp:id/menu_search";
	public static String hamburgerButton = "id:info.guardianproject.keanuapp:id/menu_sort";
	public static String threeDotsButton = "xpath://android.widget.ImageView[@content-desc='More options']";
	public static String searchEditor = "id:info.guardianproject.keanuapp:id/search_src_text";
	public static String searchResultVerify = "xpath://android.widget.TextView[@text='replace']";
	public static String cancelSearchEditor = "id:info.guardianproject.keanuapp:id/search_close_btn";
	public static String friendsScreenTitle = "xpath://android.widget.TextView[@text='Keanu | Friends']";
	public static String discoverScreenTitle = "xpath://android.widget.TextView[@text='Keanu | Discover']";
	public static String myProfileScreenTitle = "xpath://android.widget.TextView[@text='Keanu | Me']";
	public static String chatsScreenTitle = "xpath://android.widget.TextView[@text='Keanu | Chats']";
	public static String archiveScreenTitle = "xpath://android.widget.TextView[@text='Keanu | Archive']";

	public static String activeMenuOption = "xpath://android.widget.TextView[@text='Active']";
	public static String archiveMenuOption = "xpath://android.widget.TextView[@text='Archive']";

	public static String myAccountsMenuOption = "xpath://android.widget.TextView[@text='My Accounts']";
	public static String settingsMenuOption = "xpath://android.widget.TextView[@text='Settings']";
	public static String lockAppMenuOption = "xpath://android.widget.TextView[@text='Lock the app']";
	public static String signOutMenuOption = "xpath://android.widget.TextView[@text='Sign out']";

	public static String navigateBack = "xpath:(//android.widget.ImageButton)[1]";

	public static String userName = "xpath://android.widget.TextView[contains(@resource-id,'providerName')]";
	public static String server = "xpath://android.widget.TextView[contains(@resource-id,'loginName')]";
	public static String fullScreen = "id:viewpager";

	public static String closeAppButton = "id:android:id/aerr_close";

	public AppHeaders(AppLibrary appLibrary) {
		this.appLibrary = appLibrary;
		this.driver = (AppiumDriver<MobileElement>) appLibrary.getCurrentDriverInstance();
	}

	public void randomNavigate() throws Exception {
		appLibrary.terminateApp();
		Boolean next = true;
		int i;
		try {
			for (i = 1; i < 4; i++) {
				headerNavigate();
				if (i == 3) {
					next = false;
				}
			}
			try {
				if (!(next == false)) {
					appLibrary.reopenApp();
				}
			} catch (Exception e) {
				appLibrary.reopenApp();
			}

		} catch (Exception e1) {
			appLibrary.reopenApp();
		}
	}

	public AppHeaders verifyHeaders() throws Exception {
		AppLibrary.verifyMobileElement(driver, searchHistory);
		AppLibrary.verifyMobileElement(driver, hamburgerButton);
		AppLibrary.verifyMobileElement(driver, threeDotsButton);
		return new AppHeaders(appLibrary);
	}

	public AppHeaders navigateToChat() throws Exception {
		AppLibrary.findElementForMobile(driver, chatScreen).click();
		AppLibrary.verifiyText(driver, chatsScreenTitle, "Keanu | Chats");
		verifyHeaders();
		return new AppHeaders(appLibrary);
	}

	public AppHeaders navigateToArchive() throws Exception {
		AppLibrary.findElementForMobile(driver, chatScreen).click();
		AppLibrary.verifiyText(driver, archiveScreenTitle, "Keanu | Archive");
		verifyHeaders();
		return new AppHeaders(appLibrary);
	}

	public void headerNavigate() {
		AppLibrary.findElementForMobile(driver, friendScreen).click();
		AppLibrary.findElementForMobile(driver, discoverScreen).click();
		AppLibrary.findElementForMobile(driver, myProfileScreen).click();
		AppLibrary.findElementForMobile(driver, chatScreen).click();
	}

	public AppHeaders navigateToFriends() throws Exception {
		AppLibrary.findElementForMobile(driver, friendScreen).click();
		AppLibrary.verifiyText(driver, friendsScreenTitle, "Keanu | Friends");
		verifyHeaders();
		return new AppHeaders(appLibrary);
	}

	public AppHeaders navigateToDiscover() throws Exception {
		AppLibrary.findElementForMobile(driver, discoverScreen).click();
		AppLibrary.verifiyText(driver, discoverScreenTitle, "Keanu | Discover");
		verifyHeaders();
		return new AppHeaders(appLibrary);
	}

	public AppHeaders navigateToMyProfile() throws Exception {
		AppLibrary.findElementForMobile(driver, myProfileScreen).click();
		AppLibrary.verifiyText(driver, myProfileScreenTitle, "Keanu | Me");
		verifyHeaders();
		return new AppHeaders(appLibrary);
	}

	public AppHeaders searchOption(String searchText) throws Exception {
		AppLibrary.findElementForMobile(driver, searchHistory).click();
		AppLibrary.enterMobileText(driver, searchEditor, searchText);
		AppLibrary.sleep(2000);
		AppLibrary.verifiyText(driver, searchResultVerify.replace("replace", searchText), searchText);
		return new AppHeaders(appLibrary);
	}

	public AppHeaders cancelSearch() throws Exception {
		AppLibrary.findElementForMobile(driver, cancelSearchEditor).click();
		AppLibrary.findElementForMobile(driver, navigateBack).click();
		return new AppHeaders(appLibrary);
	}

	public AppHeaders hamburgerMenuOption() {
		AppLibrary.findElementForMobile(driver, hamburgerButton).click();
		AppLibrary.findElementForMobile(driver, activeMenuOption);
		AppLibrary.findElementForMobile(driver, archiveMenuOption);
		return new AppHeaders(appLibrary);
	}

	public AppHeaders navigateToActiveChats() {
		AppLibrary.findElementForMobile(driver, hamburgerButton).click();
		AppLibrary.findElementForMobile(driver, activeMenuOption).click();
		return new AppHeaders(appLibrary);
	}

	public AppHeaders navigateToArchiveChats() {
		AppLibrary.findElementForMobile(driver, hamburgerButton).click();
		AppLibrary.findElementForMobile(driver, archiveMenuOption).click();
		return new AppHeaders(appLibrary);
	}

	public AppHeaders myAccount() {
		AppLibrary.findElementForMobile(driver, threeDotsButton).click();
		AppLibrary.findElementForMobile(driver, myAccountsMenuOption);
		return new AppHeaders(appLibrary);
	}

	public AppHeaders settings() {
		AppLibrary.findElementForMobile(driver, threeDotsButton).click();
		AppLibrary.findElementForMobile(driver, settingsMenuOption).click();
		return new AppHeaders(appLibrary);
	}

	public AppHeaders lockApp() {
		AppLibrary.findElementForMobile(driver, threeDotsButton).click();
		AppLibrary.findElementForMobile(driver, lockAppMenuOption).click();
		return new AppHeaders(appLibrary);
	}

	public AppHeaders signOut() {
		AppLibrary.findElementForMobile(driver, threeDotsButton).click();
		AppLibrary.findElementForMobile(driver, signOutMenuOption).click();
		return new AppHeaders(appLibrary);
	}

	public AppHeaders threeDotsMenuOption() {
		AppLibrary.findElementForMobile(driver, hamburgerButton).click();
		AppLibrary.findElementForMobile(driver, myAccountsMenuOption);
		AppLibrary.findElementForMobile(driver, settingsMenuOption);
		AppLibrary.findElementForMobile(driver, lockAppMenuOption);
		AppLibrary.findElementForMobile(driver, signOutMenuOption);
		return new AppHeaders(appLibrary);
	}

	public AppHeaders myAccountVerification(String un, String serverUsed) {
		AppLibrary.findElementForMobile(driver, threeDotsButton).click();
		AppLibrary.findElementForMobile(driver, myAccountsMenuOption).click();
		AppLibrary.verifiyContains(driver, userName, un);
		AppLibrary.verifiyText(driver, server, "Online - " + serverUsed);
		AppLibrary.sleep(2000);
		return new AppHeaders(appLibrary);

	}

	public void navigateChatToFriend() {
		appLibrary.horizontalRightSwipe(fullScreen);
	}

	public void navigateFriendToDiscover() throws Exception {
		appLibrary.horizontalRightSwipe(fullScreen);
	}

	public void navigateDiscoverToMyProfile() throws Exception {
		appLibrary.horizontalRightSwipe(fullScreen);
	}

	public void navigateMyProfileToDiscover() throws Exception {
		appLibrary.horizontalLeftSwipe(fullScreen);
	}

	public void navigateDiscoverToFriend() throws Exception {
		appLibrary.horizontalLeftSwipe(fullScreen);
	}

	public void navigateFriendToChat() throws Exception {
		appLibrary.horizontalLeftSwipe(fullScreen);
	}

	public void verifyChatsScreen() throws Exception {
		AppLibrary.verifiyText(driver, chatsScreenTitle, "Keanu | Chats");
		verifyHeaders();
	}

	public void verifyFriendScreen() throws Exception {
		AppLibrary.verifiyText(driver, friendsScreenTitle, "Keanu | Friends");
		verifyHeaders();
	}

	public void verifyDiscoverScreen() throws Exception {
		AppLibrary.verifiyText(driver, discoverScreenTitle, "Keanu | Discover");
		verifyHeaders();
	}

	public void verifyMyProfileScreen() throws Exception {
		AppLibrary.verifiyText(driver, myProfileScreenTitle, "Keanu | Me");
		verifyHeaders();
	}

	public void navBack() throws Exception {
		driver.navigate().back();
	}
}
