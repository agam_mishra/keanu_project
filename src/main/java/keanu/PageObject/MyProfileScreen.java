package keanu.PageObject;

import java.util.List;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import keanu.Library.AppLibrary;

public class MyProfileScreen {

	private AppLibrary appLibrary;
	AppiumDriver<MobileElement> driver;

	public static String userName = "xpath://android.widget.TextView[contains(@resource-id,'tvNickname')]";
	public static String userNameDevServer = "xpath://android.widget.TextView[contains(@resource-id,'edtName')]";
	public static String userPassword = "id:info.guardianproject.keanuapp:id/edtPass";
	public static String editUserName = "xpath://android.widget.ImageView[contains(@resource-id,'edit_account_nickname')]";
	public static String editPassword = "xpath://android.widget.ImageView[contains(@resource-id,'edit_account_password')]";
	public static String pwdShowButton = "id:info.guardianproject.keanuapp:id/btnShowPass";
	public static String profilePicture = "id:info.guardianproject.keanuapp:id/imageAvatar";
	public static String camera = "xpath://android.widget.TextView[contains(@text,'Camera')]";
	public static String gallery = "xpath://android.widget.TextView[contains(@text,'Gallery')]";
	public static String files = "xpath://android.widget.TextView[contains(@text,'Files')]";
	public static String clickOnCamera = "xpath://android.widget.ImageView[@content-desc='Shutter']";
	public static String cameraClickDone = "xpath://android.widget.ImageButton[@content-desc='Done']";
	public static String cancelButton = "xpath://android.widget.Button[contains(@text,'CANCEL')]";
	public static String OKButton = "xpath://android.widget.Button[contains(@text,'OK')]";
	public static String clickOnFile = "xpath://android.widget.ImageView[contains(@instance,'4')]";
	public static String deviceAndKeysButton = "id:info.guardianproject.keanuapp:id/btnViewDevices";
	public static String deviceList = "xpath://android.widget.TextView[contains(@text,'Keanu-')]";

	public MyProfileScreen(AppLibrary appLibrary) {
		this.appLibrary = appLibrary;
		this.driver = (AppiumDriver<MobileElement>) appLibrary.getCurrentDriverInstance();
	}

	public void deviceKeyVerify(Boolean signUp, Boolean signIn) {
		AppLibrary.findElementForMobile(driver, deviceAndKeysButton).click();
		List<MobileElement> list = AppLibrary.findElementsForMobile(driver, deviceList);
		int deivceCount = list.size();
		if (signUp)
			assert deivceCount < 2;
		if (signIn)
			assert deivceCount > 1;
	}

	public MyProfileScreen userVerification(String un, String password) {
		AppLibrary.verifiyText(driver, userName, un);
		AppLibrary.findElementForMobile(driver, pwdShowButton).click();
		AppLibrary.verifiyText(driver, userPassword, password);
		AppLibrary.findElementForMobile(driver, pwdShowButton).click();
		return new MyProfileScreen(appLibrary);
	}

	public void addProfilePictureViaCamera() {
		AppLibrary.findElementForMobile(driver, profilePicture).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, camera).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, clickOnCamera).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, cameraClickDone).click();
		AppLibrary.findElementForMobile(driver, cancelButton).click();

		AppLibrary.findElementForMobile(driver, profilePicture).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, camera).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, clickOnCamera).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, cameraClickDone).click();
		AppLibrary.findElementForMobile(driver, OKButton).click();
	}

	public void addProfilePictureViaFile() {
		AppLibrary.findElementForMobile(driver, profilePicture).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, files).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, clickOnFile).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, cancelButton).click();

		AppLibrary.findElementForMobile(driver, profilePicture).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, files).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, clickOnFile).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, OKButton).click();
	}

	public void navBack() throws Exception {
		driver.navigate().back();
	}
}
