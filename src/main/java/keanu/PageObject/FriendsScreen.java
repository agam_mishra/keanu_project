package keanu.PageObject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import keanu.Library.AppLibrary;

public class FriendsScreen {

	private AppLibrary appLibrary;
	AppiumDriver<MobileElement> driver;

	public static String checkFriend = "xpath://android.widget.TextView[contains(@text,'@replace:neo.keanu.im')]";
	public static String startChat = "id:info.guardianproject.keanuapp:id/btnStartChat";
	public static String groupInfo = "id:info.guardianproject.keanuapp:id/menu_group_info";
	public static String groupName = "id:info.guardianproject.keanuapp:id/tvGroupName";
	public static String editGroupSubject = "id:info.guardianproject.keanuapp:id/edit_group_subject";
	public static String editGroupTextField = "class:android.widget.EditText";
	public static String clickOKButton = "xpath://android.widget.Button[@text='OK']";
	public static String searchHistory = "id:info.guardianproject.keanuapp:id/menu_search";

	// Delete Contact
	public static String kebabMenu = "xpath://android.widget.ImageView[@content-desc='More options']";
	public static String deleteContact = "xpath://android.widget.TextView[contains(@text,'Delete Contact')]";
	public static String deleteContactPopup = "id:info.guardianproject.keanuapp:id/alertTitle";
	public static String deleteContactMessage = "id:android:id/message";
	public static String okButton = "xpath://android.widget.Button[contains(@text,'OK')]";

	// Add Friend
	public static String clickOnAddFriend = "id:info.guardianproject.keanuapp:id/fab";
	public static String findFriendTextField = "id:info.guardianproject.keanuapp:id/email";
	public static String addFriendButton = "id:info.guardianproject.keanuapp:id/btnAddFriend";
	public static String clickOnParticularUser = "xpath://android.widget.LinearLayout//android.widget.TextView[@text='replace']";

	public FriendsScreen(AppLibrary appLibrary) {
		this.appLibrary = appLibrary;
		this.driver = (AppiumDriver<MobileElement>) appLibrary.getCurrentDriverInstance();
	}

	public void addFriend(String friendName) {
		AppLibrary.findElementForMobile(driver, clickOnAddFriend).click();
		AppLibrary.enterMobileText(driver, findFriendTextField, friendName);
		AppLibrary.sleep(5000);
		AppLibrary.findElementForMobile(driver, addFriendButton).click();
		AppLibrary.sleep(5000);
		driver.navigate().back();
	}

	public void selectParticularUser(String friendName) {
		AppLibrary.findElementForMobile(driver, clickOnParticularUser.replace("replace", friendName)).click();
	}

	public void verifyUserAbsence(String friendName) {
		AppLibrary.verifyAbsent(driver, clickOnParticularUser.replace("replace", friendName));
	}

	public void deleteContact(String un) {
		AppLibrary.findElementForMobile(driver, kebabMenu).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, deleteContact).click();
		AppLibrary.verifiyText(driver, deleteContactPopup, "Delete Contact");
		AppLibrary.verifiyContains(driver, deleteContactMessage, un);
		AppLibrary.verifiyContains(driver, deleteContactMessage, "will be deleted.");
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, okButton).click();
	}

	public void selectFriendAndCreateChatRoom(String friend, String roomName,String verifyText) {
		try {
			AppLibrary.findElementForMobile(driver, checkFriend.replace("replace", friend)).click();
			AppLibrary.verifiyText(driver, checkFriend.replace("replace", friend), verifyText);
			AppLibrary.findElementForMobile(driver, startChat).click();

			AppLibrary.findElementForMobile(driver, groupInfo).click();
			AppLibrary.findElementForMobile(driver, editGroupSubject).click();
			AppLibrary.enterMobileText(driver, editGroupTextField, roomName);
			AppLibrary.findElementForMobile(driver, clickOKButton).click();
			AppLibrary.sleep(5000);
			navBack();
		} catch (Exception e) {
			createChatRoomBySearchingFriend(friend, roomName);
		}
	}

	public void createChatRoomBySearchingFriend(String friendName, String RoomName) {
		AppLibrary.findElementForMobile(driver, clickOnAddFriend).click();
		AppLibrary.findElementForMobile(driver, addFriendButton).click();
		AppLibrary.enterMobileText(driver, findFriendTextField, friendName);
		AppLibrary.findElementForMobile(driver, addFriendButton).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, groupInfo).click();
		AppLibrary.findElementForMobile(driver, editGroupSubject).click();
		AppLibrary.enterMobileText(driver, editGroupTextField, RoomName);
		AppLibrary.findElementForMobile(driver, clickOKButton).click();
		AppLibrary.sleep(3000);
		navBack();
	}

	public void navBack() {
		driver.navigate().back();
		driver.navigate().back();
		try {
			AppLibrary.findElementForMobile(driver, searchHistory);
		} catch (Exception e) {
			driver.navigate().back();
			AppLibrary.findElementForMobile(driver, searchHistory);
		}
	}
}
