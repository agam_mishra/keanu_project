package keanu.PageObject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import keanu.Library.AppLibrary;

public class SignUpSignInScreen {

	private AppLibrary appLibrary;
	AppiumDriver<MobileElement> driver;

	// Account Selection Locators (New or existing account)
	public static String createAnAccountButton = "xpath://android.widget.Button[@resource-id='info.guardianproject.keanuapp:id/btnShowRegister']";
	public static String signInToExistingAccButton = "xpath://android.widget.Button[@resource-id='info.guardianproject.keanuapp:id/btnShowLogin']";

	// Sign Up Locators
	public static String createIDTitle = "xpath://android.widget.TextView[@text='Create an ID']";
	public static String userIDTextField = "xpath://android.widget.EditText[@resource-id='info.guardianproject.keanuapp:id/edtNameAdvanced']";
	public static String enterPwdTextField = "xpath://android.widget.EditText[@resource-id='info.guardianproject.keanuapp:id/edtNewPass']";
	public static String confirmPwdTextField = "xpath://android.widget.EditText[@resource-id='info.guardianproject.keanuapp:id/edtNewPassConfirm']";
	public static String registerButton = "xpath://android.widget.Button[@resource-id='info.guardianproject.keanuapp:id/btnNewRegister']";

	// Sign In Locators
	public static String existingAccountMessage = "xpath://android.widget.TextView[@text='You can use an existing account on any server you would like.']";
	public static String usernameTextField = "xpath://android.widget.EditText[contains(@resource-id,'edtName')]";
	public static String pwdTextField = "xpath://android.widget.EditText[contains(@resource-id,'edtPass')]";
	public static String serverTextField = "xpath://android.widget.EditText[contains(@resource-id,'edtServer')]";
	public static String signInButton = "xpath://android.widget.Button[contains(@resource-id,'btnSignIn')]";

	public SignUpSignInScreen(AppLibrary appLibrary) {
		this.appLibrary = appLibrary;
		this.driver = (AppiumDriver<MobileElement>) appLibrary.getCurrentDriverInstance();
	}

	public SignUpSignInScreen accountSelection(Boolean existingAccount) {
		if (existingAccount)
			AppLibrary.findElementForMobile(driver, signInToExistingAccButton).click();
		else
			AppLibrary.findElementForMobile(driver, createAnAccountButton).click();
		return new SignUpSignInScreen(appLibrary);
	}

	public SignUpSignInScreen signIn(String username, String password, String serverName) throws Exception {

		try {
			AppLibrary.verifiyText(driver, existingAccountMessage,
					"You can use an existing account on any server you would like.");
			AppLibrary.enterMobileText(driver, usernameTextField, username);
			AppLibrary.enterMobileText(driver, pwdTextField, password);
			AppLibrary.enterMobileText(driver, serverTextField, serverName);
			AppLibrary.sleep(2000);
			AppLibrary.findElementForMobile(driver, signInButton).click();
			AppLibrary.sleep(5000);
		} catch (Exception e) {
			appLibrary.openApp();
			AppLibrary.verifiyText(driver, existingAccountMessage,
					"You can use an existing account on any server you would like.");
			AppLibrary.enterMobileText(driver, usernameTextField, username);
			AppLibrary.enterMobileText(driver, pwdTextField, password);
			AppLibrary.enterMobileText(driver, serverTextField, serverName);
			AppLibrary.findElementForMobile(driver, signInButton).click();
		}
		appLibrary.terminateApp();
		return new SignUpSignInScreen(appLibrary);
	}

	public SignUpSignInScreen signUp(String username, String password) throws Exception {
		try {
			AppLibrary.verifiyText(driver, createIDTitle, "Create an ID");
			AppLibrary.enterMobileText(driver, userIDTextField, username);
			AppLibrary.enterMobileText(driver, enterPwdTextField, password);
			AppLibrary.enterMobileText(driver, confirmPwdTextField, password);
			AppLibrary.sleep(2000);
			// AppLibrary.findElementForMobile(driver, registerButton).click();
			AppLibrary.sleep(5000);
		} catch (Exception e) {
			appLibrary.openApp();
			AppLibrary.verifiyText(driver, createIDTitle, "Create an ID");
			AppLibrary.enterMobileText(driver, userIDTextField, username);
			AppLibrary.enterMobileText(driver, enterPwdTextField, password);
			AppLibrary.enterMobileText(driver, confirmPwdTextField, password);
			// AppLibrary.findElementForMobile(driver, registerButton).click();
		}
		appLibrary.openApp();
		return new SignUpSignInScreen(appLibrary);
	}
}
