package keanu.PageObject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;
import keanu.Library.AppLibrary;

public class AdditionalFunctionalityScreen {
	private AppLibrary appLibrary;
	AppiumDriver<MobileElement> driver;

	public static String moreOptions = "xpath://android.widget.ImageView[@content-desc='More options']";
	public static String myAccountOption = "xpath://android.widget.TextView[contains(@text,'My Accounts')]";
	public static String settingsOption = "xpath://android.widget.TextView[contains(@text,'Settings')]";
	public static String lockAppOption = "xpath://android.widget.TextView[contains(@text,'Lock the app')]";
	public static String signOutOption = "xpath://android.widget.TextView[contains(@text,'Sign out')]";

	// My Accoutns
	public static String addAccount = "id:info.guardianproject.keanuapp:id/menu_add_account";
	public static String serverVerify = "xpath:(//android.widget.TextView[contains(@text,'neo.keanu.im')])[1]";
	public static String acconutOnOffSwitch = "xpath:(//android.widget.Switch)[1]";
	public static String availableUser = "xpath://android.widget.TextView[contains(@text,'replace')]";

	// Settings
	public static String language = "xpath://android.widget.TextView[contains(@text,'Languages')]";
	public static String myData = "xpath://android.widget.TextView[contains(@text,'Protect My Data')]";
	public static String panicTrigger = "xpath://android.widget.TextView[contains(@text,'Panic Trigger App')]";
	public static String panicMessage = "xpath://android.widget.TextView[contains(@text,'A panic app can trigger the app to')]";
	public static String panicSetup = "xpath://android.widget.TextView[contains(@text,'Panic Setup')]";
	public static String panicNotification = "xpath://android.widget.TextView[contains(@text,'Set up how the app reacts to a panic notification.')]";
	public static String customColour = "xpath://android.widget.TextView[contains(@text,'Custom Colors')]";
	public static String headerColours = "xpath://android.widget.TextView[contains(@text,'Header Colors')]";
	public static String headerColourMessage = "xpath://android.widget.TextView[contains(@text,'Color of headers, actionbars and toolbars')]";
	public static String textColour = "xpath://android.widget.TextView[contains(@text,'Text Color')]";
	public static String textColourMessage = "xpath://android.widget.TextView[contains(@text,'Color of text in messages and lists')]";
	public static String backgroundColour = "xpath://android.widget.TextView[contains(@text,'Background Color')]";
	public static String backgroundColourMessage = "xpath://android.widget.TextView[contains(@text,'Color of chat backgrounds and lists')]";
	public static String resetColour = "xpath://android.widget.TextView[contains(@text,'Reset Colors')]";
	public static String resetColourMessage = "xpath://android.widget.TextView[contains(@text,'Reset colors to default values')]";
	public static String autoStart = "xpath://android.widget.TextView[contains(@text,'Start Automatically')]";
	public static String autoStartMessage = "xpath://android.widget.TextView[contains(@text,'Always start and automatically')]";

	public static String NotificationSetting = "xpath://android.widget.TextView[contains(@text,'Notification settings')]";
	public static String imNotification = "xpath://android.widget.TextView[contains(@text,'IM notifications')]";
	public static String notificationMsg = "xpath://android.widget.TextView[contains(@text,'Notify in status bar when IM arrives')]";
	public static String viberateOption = "xpath://android.widget.TextView[contains(@text,'Vibrate')]";
	public static String viberateMsg = "xpath://android.widget.TextView[contains(@text,'Also vibrate when IM arrives')]";
	public static String soundOption = "xpath://android.widget.TextView[contains(@text,'Sound')]";
	public static String soundMessage = "xpath://android.widget.TextView[contains(@text,'Also play ringtone when IM arrives')]";
	public static String selectRingtone = "xpath://android.widget.TextView[contains(@text,'Select Custom Ringtone')]";
	public static String enableDebugLog = "xpath://android.widget.TextView[contains(@text,'Enable Debug Logs')]";
	public static String enableDebugLogMsg = "xpath://android.widget.TextView[contains(@text,'Output app log data to standard out / logcat for debugging')]";
	public static String blockScreenshot = "xpath://android.widget.TextView[contains(@text,'Block Screenshots')]";
	public static String blockScreenshotMessage = "xpath://android.widget.TextView[contains(@text,'Block attempts for other apps to take')]";
	public static String photoVerification = "xpath://android.widget.TextView[contains(@text,'Photo Verification')]";
	public static String photoVerificationMsg = "xpath://android.widget.TextView[contains(@text,'Add extra proof data to your photos to make')]";
	public static String snackbarMsg = "id:info.guardianproject.keanuapp:id/snackbar_text";

	public AdditionalFunctionalityScreen(AppLibrary appLibrary) {
		this.appLibrary = appLibrary;
		this.driver = (AppiumDriver<MobileElement>) appLibrary.getCurrentDriverInstance();
	}

	public void settingUI() {
		AppLibrary.verifiyText(driver, language, "Languages");
		AppLibrary.verifiyText(driver, myData, "Protect My Data");
		AppLibrary.verifiyText(driver, panicTrigger, "Panic Trigger App");
		AppLibrary.verifiyText(driver, panicMessage,
				"A panic app can trigger the app to lock, send a message, delete data, etc.");
		AppLibrary.verifiyText(driver, panicSetup, "Panic Setup");
		AppLibrary.verifiyText(driver, panicNotification, "Set up how the app reacts to a panic notification.");
		AppLibrary.verifiyText(driver, customColour, "Custom Colors");
		AppLibrary.verifiyText(driver, headerColours, "Header Colors");
		AppLibrary.verifiyText(driver, headerColourMessage, "Color of headers, actionbars and toolbars");
		AppLibrary.verifiyText(driver, textColour, "Text Color");
		AppLibrary.verifiyText(driver, textColourMessage, "Color of text in messages and lists");
		AppLibrary.verifiyText(driver, backgroundColour, "Background Color");
		AppLibrary.verifiyText(driver, backgroundColourMessage, "Color of chat backgrounds and lists");
		AppLibrary.verifiyText(driver, resetColour, "Reset Colors");
		AppLibrary.verifiyText(driver, resetColourMessage, "Reset colors to default values");
		AppLibrary.verifiyText(driver, autoStart, "Start Automatically");
		AppLibrary.verifiyText(driver, autoStartMessage,
				"Always start and automatically sign into previously logged in accounts");

		appLibrary.swipeDownByText("Add extra proof data to your photos to make it easier");
		AppLibrary.verifiyText(driver, NotificationSetting, "Notification settings");
		AppLibrary.verifiyText(driver, imNotification, "IM notifications");
		AppLibrary.verifiyText(driver, notificationMsg, "Notify in status bar when IM arrives");
		AppLibrary.verifiyText(driver, viberateOption, "Vibrate");
		AppLibrary.verifiyText(driver, viberateMsg, "Also vibrate when IM arrives");
		AppLibrary.verifiyText(driver, soundOption, "Sound");
		AppLibrary.verifiyText(driver, soundMessage, "Also play ringtone when IM arrives");
		AppLibrary.verifiyText(driver, selectRingtone, "Select Custom Ringtone");
		AppLibrary.verifiyText(driver, enableDebugLog, "Enable Debug Logs");
		AppLibrary.verifiyText(driver, enableDebugLogMsg, "Output app log data to standard out / logcat for debugging");
		AppLibrary.verifiyText(driver, blockScreenshot, "Block Screenshots");
		AppLibrary.verifiyText(driver, blockScreenshotMessage,
				"Block attempts for other apps to take screenshots of this app");
		AppLibrary.verifiyText(driver, photoVerification, "Photo Verification");
		AppLibrary.verifiyText(driver, photoVerificationMsg,
				"Add extra proof data to your photos to make it easier for people to verify the authenticity");
		driver.navigate().back();
	}

	public void clickOnMoreOption() {
		AppLibrary.sleep(2000);
		AppLibrary.findElementForMobile(driver, moreOptions).click();
	}

	public void myAccountNavigation() {
		AppLibrary.sleep(5000);
		clickOnMoreOption();
		AppLibrary.findElementForMobile(driver, myAccountOption).click();
		AppLibrary.sleep(2000);
	}

	public void settingsNavigation() {
		clickOnMoreOption();
		AppLibrary.findElementForMobile(driver, settingsOption).click();
	}

	public void lockAppNavigation() {
		clickOnMoreOption();
		AppLibrary.findElementForMobile(driver, lockAppOption).click();
		AppLibrary.sleep(2000);
	}

	public void signOut() {
		clickOnMoreOption();
		AppLibrary.findElementForMobile(driver, signOutOption).click();
	}

	public void menuOptions() {
		AppLibrary.findElementForMobile(driver, moreOptions).click();
		AppLibrary.verifiyText(driver, myAccountOption, "My Accounts");
		AppLibrary.verifiyText(driver, settingsOption, "Settings");
		AppLibrary.verifiyText(driver, lockAppOption, "Lock the app");
		AppLibrary.verifiyText(driver, signOutOption, "Sign out");
		driver.navigate().back();
	}

	public void myAccountVerification(String un, String serverUsed) {
		String uName = AppLibrary.findElementForMobile(driver, availableUser.replace("replace", un)).getText();
		uName.contains(un);
		AppLibrary.verifiyText(driver, serverVerify, "Online - " + serverUsed);
	}

	public void deactivateAccount(String serverUsed) {
		AppLibrary.verifiyText(driver, serverVerify, "Online - " + serverUsed);
		AppLibrary.findElementForMobile(driver, acconutOnOffSwitch).click();
		AppLibrary.sleep(3000);
		AppLibrary.verifiyText(driver, serverVerify, serverUsed);
	}

	public void activateAccount(String serverUsed) {
		AppLibrary.verifiyText(driver, serverVerify, serverUsed);
		AppLibrary.findElementForMobile(driver, acconutOnOffSwitch).click();
		AppLibrary.sleep(5000);
		AppLibrary.verifiyText(driver, serverVerify, "Online - " + serverUsed);
	}

	public void accountChange(String un) {
		MobileElement user = AppLibrary.findElementForMobile(driver, availableUser.replace("replace", un));

		AndroidTouchAction touch = new AndroidTouchAction(driver);
		touch.longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(user))).release()
				.perform();

		AppLibrary.verifiyText(driver, snackbarMsg, "Default account changed");
	}

	public void navBack() {
		driver.navigate().back();
	}

	public void addAccount() {
		AppLibrary.findElementForMobile(driver, addAccount).click();
	}
}
