package keanu.PageObject;

import java.util.List;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import keanu.Library.AppLibrary;

public class ChatScreen {

	private AppLibrary appLibrary;
	AppiumDriver<MobileElement> driver;

	public static String chatRoomName = "xpath://android.widget.TextView[@text='replace']";
	public static String endPoint = "id:info.guardianproject.keanuapp:id/statusText";
	public static String searchHistory = "id:info.guardianproject.keanuapp:id/menu_search";

	public static String createChatRoomButton = "id:info.guardianproject.keanuapp:id/fab";
	public static String createGroupButton = "id:info.guardianproject.keanuapp:id/btnCreateGroup";
	public static String startChatTick = "id:info.guardianproject.keanuapp:id/action_start_chat";

	public static String findFriendTextField = "id:info.guardianproject.keanuapp:id/email";
	public static String addFriend = "xpath://android.widget.TextView[@text='Add friend']";
	public static String addFriendButton = "id:info.guardianproject.keanuapp:id/btnAddFriend";

	public static String availableFriendInList = "xpath://android.widget.TextView[@text='replace']";
	public static String selectedAvtarCheck = "xpath://android.widget.ImageView[@resource-id='info.guardianproject.keanuapp:id/avatarCheck']";

	public static String groupInfo = "id:info.guardianproject.keanuapp:id/menu_group_info";
	public static String groupName = "id:info.guardianproject.keanuapp:id/tvGroupName";
	public static String editGroupSubject = "id:info.guardianproject.keanuapp:id/edit_group_subject";
	public static String editGroupTextField = "class:android.widget.EditText";
	public static String clickOKButton = "xpath://android.widget.Button[@text='OK']";

	public static String addFriendsInGroup = "id:info.guardianproject.keanuapp:id/tvActionAddFriends";
	public static String notificationTitle = "xpath://android.widget.TextView[@text='Notifications']";
	public static String notificationTogglSwitch = "id:info.guardianproject.keanuapp:id/chkNotifications";
	public static String leaveRoom = "id:info.guardianproject.keanuapp:id/tvActionLeave";
	public static String leaveRoomPopupTitle = "id:info.guardianproject.keanuapp:id/alertTitle";
	public static String leaveRoomMessage = "id:android:id/message";
	public static String archiveRoomButton = "xpath://android.widget.Button[contains(@text,'Archive')] | //android.widget.Button[contains(@text,'ARCHIVE')]";
	public static String cancelRoomButton = "xpath://android.widget.Button[contains(@text,'Cancel')] | //android.widget.Button[contains(@text,'CANCEL')]";
	public static String leaveRoomButton = "xpath://android.widget.Button[contains(@text,'Leave')] | //android.widget.Button[contains(@text,'LEAVE')]";

	public static String clickOnParticularChatRoom = "xpath://android.widget.LinearLayout//android.widget.TextView[@text='replace']";

	public static String composeMesage = "id:info.guardianproject.keanuapp:id/composeMessage";
	public static String sendMsgButton = "id:info.guardianproject.keanuapp:id/btnSend";
	public static String attachButton = "id:info.guardianproject.keanuapp:id/btnAttach";
	public static String verifyMessage = "xpath://android.widget.TextView[@text='replace']";

	public static String attachPicture = "id:info.guardianproject.keanuapp:id/btnAttachPicture";
	public static String takePicture = "id:info.guardianproject.keanuapp:id/btnTakePicture";
	public static String attachSticker = "id:info.guardianproject.keanuapp:id/btnAttachSticker";
	public static String attachAudio = "id:info.guardianproject.keanuapp:id/btnAttachAudio";
	public static String attachFiles = "id:info.guardianproject.keanuapp:id/btnAttachFile";

	public static String whaleSticker = "xpath://android.widget.ImageView[@content-desc='success-whale.png']";
	public static String windSailSticker = "xpath://android.widget.ImageView[@content-desc='windsail.png']";
	public static String verifySticker = "xpath:(//android.widget.ImageView[@resource-id='info.guardianproject.keanuapp:id/media_thumbnail'])[1]";
	public static String micButton = "id:info.guardianproject.keanuapp:id/btnMic";
	public static String holdToTalkOnMic = "id:info.guardianproject.keanuapp:id/buttonHoldToTalk";
	public static String playButton = "xpath:(//android.widget.ImageView[@resource-id='info.guardianproject.keanuapp:id/play'])[1]";
	public static String seekBarButton = "xpath://android.widget.SeekBar[@instance='0']";
	public static String chatGroupsList = "xpath://android.widget.TextView[contains(@text,'You have been invited')]";
	public static String chatGroups = "xpath:(//android.widget.TextView[contains(@text,'invited to chat')])[replace]";
	public static String joinGroup = "id:info.guardianproject.keanuapp:id/btnJoinAccept";
	public static String declineJoiningGroup = "id:info.guardianproject.keanuapp:id/btnJoinDecline";
	public static String clickOnFriend = "xpath:(//android.widget.TextView[contains(@text,'replace')])[1]";
	public static String checkMembers = "xpath://android.widget.TextView[contains(@text,'@replace:neo.keanu.im')]";
	public static String checkSelfStatus = "xpath://android.widget.TextView[contains(@text,'replace (you)')]";

	public static String findYourFiend = "xpath://android.widget.TextView[contains(@text,'Find your friend')]";
	public static String inviteLinkOptions = "xpath://android.widget.TextView[contains(@text,'Send your invite link')]";
	public static String shareOptions = "xpath://android.widget.TextView[contains(@text,'Standing next to them?')]";

	public static String chooseAFriendTitle = "xpath://android.widget.TextView[contains(@text,'Choose a Friend')]";

	public ChatScreen(AppLibrary appLibrary) {
		this.appLibrary = appLibrary;
		this.driver = (AppiumDriver<MobileElement>) appLibrary.getCurrentDriverInstance();
	}

	public void verifyfindFriendUI(String userName) throws Exception {
		AppLibrary.findElementForMobile(driver, createChatRoomButton).click();
		AppLibrary.sleep(2000);
		try {
			AppLibrary.findElementForMobile(driver, addFriend).click();
			AppLibrary.sleep(2000);
		} catch (Exception e) {
			// no friend in contacts available so add friend option is not
			// available
		}
		appLibrary.openApp();
		AppLibrary.verifiyText(driver, findYourFiend, "Find your friend");
		AppLibrary.verifiyText(driver, inviteLinkOptions, "Send your invite link");
		AppLibrary.verifiyText(driver, shareOptions, "Standing next to them?");
		navigateBack();
		try {
			AppLibrary.findElementForMobile(driver, chooseAFriendTitle);
			navigateBack();
		} catch (Exception e) {
			//On main screen
		}
		appLibrary.openApp();
	}

	public void checkMemberList(String chatGroupName, String userName, Boolean self) {
		navigateToChatRoom(chatGroupName);
		AppLibrary.findElementForMobile(driver, groupInfo).click();
		AppLibrary.verifiyText(driver, checkMembers.replace("replace", userName), "@" + userName + ":neo.keanu.im");
		if (self)
			AppLibrary.verifiyText(driver, checkSelfStatus.replace("replace", userName), userName + " (you)");
		navBack();
	}

	public void joinChatGroups() throws Exception {
		AppLibrary.sleep(5000);
		appLibrary.terminateApp();
		try {
			List<MobileElement> list = AppLibrary.findElementsForMobile(driver, chatGroupsList);
			int size = list.size();
			for (int i = 0; i < size; i++) {
				AppLibrary.findElementForMobile(driver, chatGroups.replace("replace", Integer.toString(i + 1))).click();
				try {
					AppLibrary.findElementForMobile(driver, joinGroup).click();
					driver.navigate().back();
				} catch (Exception e) {
					driver.navigate().back();
				}
			}
			AppLibrary.sleep(5000);
		} catch (Exception e) {
			appLibrary.reopenApp();
			List<MobileElement> list = AppLibrary.findElementsForMobile(driver, chatGroupsList);
			int size = list.size();
			for (int i = 0; i < size; i++) {
				AppLibrary.findElementForMobile(driver, chatGroups.replace("replace", Integer.toString(i + 1))).click();
				try {
					AppLibrary.findElementForMobile(driver, joinGroup).click();
					driver.navigate().back();
				} catch (Exception e1) {
					driver.navigate().back();
				}
			}
			AppLibrary.sleep(5000);
		}
	}

	public void composeAndSendMessage(String msg1) {
		AppLibrary.enterMobileText(driver, composeMesage, msg1);
		AppLibrary.findElementForMobile(driver, sendMsgButton).click();
		AppLibrary.sleep(10000);
	}

	public void attachments(Boolean sticker1, Boolean sticker2) {
		AppLibrary.findElementForMobile(driver, attachButton).click();
		if (sticker1) {
			AppLibrary.findElementForMobile(driver, attachSticker).click();
			AppLibrary.findElementForMobile(driver, whaleSticker).click();
			AppLibrary.sleep(10000);
		}
		if (sticker2) {
			AppLibrary.findElementForMobile(driver, attachSticker).click();
			AppLibrary.findElementForMobile(driver, windSailSticker).click();
			AppLibrary.sleep(10000);
		}
	}

	public void verifySendMessage(String msg) {
		AppLibrary.verifiyText(driver, verifyMessage.replace("replace", msg), msg);
	}

	public void verifySendStickers() {
		AppLibrary.findElementForMobile(driver, verifySticker);
	}

	public void verifyVoiceMessage() {
		AppLibrary.findElementForMobile(driver, playButton);
		AppLibrary.findElementForMobile(driver, seekBarButton);
	}

	public void sendVoiceMsg() {
		AppLibrary.findElementForMobile(driver, micButton).click();
		appLibrary.horizontalSwipe(holdToTalkOnMic, sendMsgButton);
		AppLibrary.sleep(10000);
	}

	public void createChatRoomBySearchingFriend(String friendName, String RoomName) throws Exception {
		AppLibrary.findElementForMobile(driver, createChatRoomButton).click();
		AppLibrary.findElementForMobile(driver, addFriendButton).click();
		AppLibrary.enterMobileText(driver, findFriendTextField, friendName);
		AppLibrary.findElementForMobile(driver, addFriendButton).click();
		AppLibrary.sleep(3000);

		try {
			AppLibrary.findElementForMobile(driver, groupInfo).click();
			AppLibrary.findElementForMobile(driver, editGroupSubject).click();
			AppLibrary.enterMobileText(driver, editGroupTextField, RoomName);
			AppLibrary.findElementForMobile(driver, clickOKButton).click();
			AppLibrary.sleep(3000);
			navBack();
		} catch (Exception e) {
			appLibrary.reopenApp();
			AppLibrary.findElementForMobile(driver, groupInfo).click();
			AppLibrary.findElementForMobile(driver, editGroupSubject).click();
			AppLibrary.enterMobileText(driver, editGroupTextField, RoomName);
			AppLibrary.findElementForMobile(driver, clickOKButton).click();
			AppLibrary.sleep(3000);
			navBack();
		}
	}

	public void navigateToChatRoom(String chatRoom) {
		AppLibrary.findElementForMobile(driver, clickOnParticularChatRoom.replace("replace", chatRoom)).click();
	}

	public void editChatRoomName(String chatGroupName, String editedName) {
		navigateToChatRoom(chatGroupName);
		AppLibrary.findElementForMobile(driver, groupInfo).click();
		AppLibrary.verifiyText(driver, groupName, chatGroupName);
		AppLibrary.findElementForMobile(driver, editGroupSubject).click();
		AppLibrary.enterMobileText(driver, editGroupTextField, editedName);
		AppLibrary.findElementForMobile(driver, clickOKButton).click();
		AppLibrary.sleep(5000);
		navBack();
	}

	public void addingFriendInExistingRoom(String chatGroupName, String friendName) {
		navigateToChatRoom(chatGroupName);
		AppLibrary.findElementForMobile(driver, groupInfo).click();
		AppLibrary.findElementForMobile(driver, addFriendsInGroup).click();
		AppLibrary.findElementForMobile(driver, addFriend).click();
		AppLibrary.enterMobileText(driver, findFriendTextField, friendName);
		AppLibrary.findElementForMobile(driver, addFriendButton).click();
		AppLibrary.sleep(3000);
		navBack();
	}

	public void leaveRoom(String chatGroupName) {
		navigateToChatRoom(chatGroupName);
		AppLibrary.findElementForMobile(driver, groupInfo).click();
		AppLibrary.sleep(2000);
		AppLibrary.findElementForMobile(driver, leaveRoom).click();
		AppLibrary.verifiyText(driver, leaveRoomMessage,
				"Your group chat history will be wiped away. To keep these chats, archive the group instead.");
		AppLibrary.verifiyText(driver, leaveRoomPopupTitle, "Leave Group?");

		AppLibrary.findElementForMobile(driver, cancelRoomButton).click();
		AppLibrary.sleep(2000);
		AppLibrary.findElementForMobile(driver, leaveRoom).click();
		AppLibrary.verifiyText(driver, leaveRoomMessage,
				"Your group chat history will be wiped away. To keep these chats, archive the group instead.");
		AppLibrary.verifiyText(driver, leaveRoomPopupTitle, "Leave Group?");

		AppLibrary.findElementForMobile(driver, leaveRoomButton).click();
		AppLibrary.sleep(2000);
		verifyChatRoomAbsence(chatGroupName);
	}

	public void archiveChatRoomFromChatInfo(String chatGroupName) {
		navigateToChatRoom(chatGroupName);
		AppLibrary.enterMobileText(driver,composeMesage, chatGroupName);
		AppLibrary.findElementForMobile(driver, sendMsgButton).click();
		AppLibrary.findElementForMobile(driver, groupInfo).click();
		AppLibrary.sleep(2000);
		AppLibrary.findElementForMobile(driver, leaveRoom).click();
		AppLibrary.verifiyText(driver, leaveRoomMessage,
				"Your group chat history will be wiped away. To keep these chats, archive the group instead.");
		AppLibrary.verifiyText(driver, leaveRoomPopupTitle, "Leave Group?");
		AppLibrary.findElementForMobile(driver, archiveRoomButton).click();
	}

	public void navBack() {
		driver.navigate().back();
		driver.navigate().back();
		try {
			AppLibrary.findElementForMobile(driver, searchHistory);
		} catch (Exception e) {
			driver.navigate().back();
			AppLibrary.findElementForMobile(driver, searchHistory);
		}
	}

	public void navigateBack() {
		driver.navigate().back();
	}

	public void archiveChatRoom(String RoomName) {
		appLibrary.horizontalSwipe(chatRoomName.replace("replace", RoomName), endPoint);
	}

	public void verifyChatRoom(String RoomName) throws Exception {
		AppLibrary.findElementForMobile(driver, chatRoomName.replace("replace", RoomName));
	}

	public void verifyChatRoomAbsence(String RoomName) {
		AppLibrary.verifyAbsent(driver, chatRoomName.replace("replace", RoomName));
	}

	public void unArchiveChatRoom(String RoomName) {
		appLibrary.horizontalSwipe(chatRoomName.replace("replace", RoomName), endPoint);
	}
}
