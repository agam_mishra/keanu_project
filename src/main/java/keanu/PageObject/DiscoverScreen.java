package keanu.PageObject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import keanu.Library.AppLibrary;

public class DiscoverScreen {

	private AppLibrary appLibrary;
	AppiumDriver<MobileElement> driver;

	public static String navToServices = "xpath://android.widget.TextView[contains(@text,'Services')]";
	public static String navToPhotoStream = "xpath://android.widget.TextView[contains(@text,'Photo Stream')]";
	public static String navToCreateGroup = "xpath://android.widget.TextView[contains(@text,'Create Group (Room)')]";
	public static String navToShareSticker = "xpath://android.widget.TextView[contains(@text,'Sticker Share')]";
	public static String navToChangeTheme = "xpath://android.widget.TextView[contains(@text,'Change Theme')]";
	public static String editGroupSubject = "id:info.guardianproject.keanuapp:id/edit_group_subject";
	public static String editGroupTextField = "class:android.widget.EditText";
	public static String clickOKButton = "xpath://android.widget.Button[@text='OK']";
	public static String searchHistory = "id:info.guardianproject.keanuapp:id/menu_search";

	public static String waleSticket = "xpath://android.widget.ImageView[@content-desc='success-whale.png']";
	
	public DiscoverScreen(AppLibrary appLibrary) {
		this.appLibrary = appLibrary;
		this.driver = (AppiumDriver<MobileElement>) appLibrary.getCurrentDriverInstance();
	}
	
	public void selectSticker(){
		AppLibrary.findElementForMobile(driver, waleSticket).click();
	}

	public void discScreenUI(){
		AppLibrary.verifiyText(driver, navToServices, "Services");
		AppLibrary.verifiyText(driver, navToPhotoStream, "Photo Stream");
		AppLibrary.verifiyText(driver, navToCreateGroup, "Create Group (Room)");
		AppLibrary.verifiyText(driver, navToShareSticker, "Sticker Share");
		AppLibrary.verifiyText(driver, navToChangeTheme, "Change Theme");
	}
	
	public void navigateToServices() {
		AppLibrary.findElementForMobile(driver, navToServices).click();
		AppLibrary.sleep(2000);
		AppLibrary.verifiyText(driver, navToServices, "Services");
	}

	public void navigateToPhotoStream() {
		AppLibrary.findElementForMobile(driver, navToPhotoStream).click();
		AppLibrary.sleep(2000);
		AppLibrary.verifiyText(driver, navToPhotoStream, "Photo Stream");
	}

	public void createAGroup(String roomName) {
		AppLibrary.findElementForMobile(driver, navToCreateGroup).click();
		updateChatRoom(roomName);
	}

	public void navigateToShareSticker() {
		AppLibrary.findElementForMobile(driver, navToShareSticker).click();
		AppLibrary.sleep(2000);
		AppLibrary.verifiyText(driver, navToShareSticker, "Sticker Share");
	}

	public void navigateToChangeTheme() {
		AppLibrary.findElementForMobile(driver, navToChangeTheme);
	}

	public void navBack() {
		driver.navigate().back();
	}

	public void updateChatRoom(String roomName) {
		AppLibrary.findElementForMobile(driver, editGroupSubject).click();
		AppLibrary.enterMobileText(driver, editGroupTextField, roomName);
		AppLibrary.findElementForMobile(driver, clickOKButton).click();
		AppLibrary.sleep(3000);
		navToBack();
	}

	public void navToBack() {
		driver.navigate().back();
		driver.navigate().back();
		try {
			AppLibrary.findElementForMobile(driver, searchHistory);
		} catch (Exception e) {
			driver.navigate().back();
			AppLibrary.findElementForMobile(driver, searchHistory);
		}
	}
}
