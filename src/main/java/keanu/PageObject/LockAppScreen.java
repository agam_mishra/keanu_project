package keanu.PageObject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import keanu.Library.AppLibrary;

public class LockAppScreen {

	private AppLibrary appLibrary;
	AppiumDriver<MobileElement> driver;

	public static String moreOptions = "xpath://android.widget.ImageView[@content-desc='More options']";
	public static String appLockOption = "xpath://android.widget.TextView[contains(@text,'Lock the app')]";
	public static String resetLockOption = "xpath://android.widget.TextView[contains(@text,'Reset Lock')]";

	public static String logoSticker = "id:info.guardianproject.keanuapp:id/imageLogo";
	public static String setPhraseText = "xpath://android.widget.TextView[contains(@text,'Please set a passphrase to lock the app')]";
	public static String skipButton = "id:info.guardianproject.keanuapp:id/btnSkip";

	public static String pwdButton = "id:info.guardianproject.keanuapp:id/btnCreate";
	public static String pwdPlaceHolder = "id:info.guardianproject.keanuapp:id/editNewPassphrase";
	public static String confirmPwdPlaceHolder = "id:info.guardianproject.keanuapp:id/editConfirmNewPassphrase";
	public static String pwdNotMatchMsg = "xpath://android.widget.TextView[contains(@text,'Password did not match, please try again')]";

	public static String enterPwd = "xpath://android.widget.EditText[contains(@text,'Password')]";

	public LockAppScreen(AppLibrary appLibrary) {
		this.appLibrary = appLibrary;
		this.driver = (AppiumDriver<MobileElement>) appLibrary.getCurrentDriverInstance();
	}

	public void clickOnMoreOption() {
		AppLibrary.sleep(1500);
		AppLibrary.findElementForMobile(driver, moreOptions).click();
	}

	public void appLockOption() {
		AppLibrary.sleep(1500);
		AppLibrary.findElementForMobile(driver, appLockOption).click();
	}

	public void resetLockOption() {
		AppLibrary.sleep(1500);
		AppLibrary.findElementForMobile(driver, resetLockOption).click();
	}

	public void lockAppUI() {
		AppLibrary.findElementForMobile(driver, logoSticker);
		AppLibrary.verifiyText(driver, setPhraseText, "Please set a passphrase to lock the app");
	}

	public void lockAppSkip() {
		AppLibrary.findElementForMobile(driver, skipButton).click();
	}

	public void lockAppIncorrectConfirmPwd(String pwd, String confirmPwd) {
		AppLibrary.enterMobileText(driver, pwdPlaceHolder, pwd);
		AppLibrary.findElementForMobile(driver, pwdButton).click();
		AppLibrary.enterMobileText(driver, confirmPwdPlaceHolder, confirmPwd);
		AppLibrary.findElementForMobile(driver, pwdButton).click();
		AppLibrary.sleep(3000);
		AppLibrary.findElementForMobile(driver, logoSticker);
		AppLibrary.findElementForMobile(driver, pwdButton).click();
	}

	public void setLockApp(String pwd) {
		AppLibrary.enterMobileText(driver, pwdPlaceHolder, pwd);
		AppLibrary.findElementForMobile(driver, pwdButton).click();
		AppLibrary.enterMobileText(driver, confirmPwdPlaceHolder, pwd);
		AppLibrary.findElementForMobile(driver, pwdButton).click();
	}

	public void lockAndUnlockApp(String pwd) throws Exception {
		appLibrary.openApp();
		appLibrary.openApp();
		MobileElement enter = AppLibrary.findElementForMobile(driver, enterPwd);
		AppLibrary.enterMobileText(driver, enterPwd, pwd);
		enter.submit();
	}

	public void resetLock() throws Exception {
		appLibrary.openApp();
		appLibrary.openApp();
		AppLibrary.findElementForMobile(driver, resetLockOption).click();
		AppLibrary.sleep(2000);
	}
}
