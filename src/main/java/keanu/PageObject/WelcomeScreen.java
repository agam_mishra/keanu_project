package keanu.PageObject;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import keanu.Library.AppLibrary;

public class WelcomeScreen {

	private AppLibrary appLibrary;
	AppiumDriver<MobileElement> driver;

	public static String selectLanguageButton = "id:info.guardianproject.keanuapp:id/languageButton";
//	public static String skipButton = "id:info.guardianproject.keanuapp:id/nextButton";
	public static String skipButton = "xpath://android.widget.TextView[contains(@text,'Skip')]";
	public static String languageTitle = "id:android:id/alertTitle";
	public static String selectLang = "xpath://android.widget.CheckedTextView[@text='replace']";

	public WelcomeScreen(AppLibrary appLibrary) {
		this.appLibrary = appLibrary;
		this.driver = (AppiumDriver<MobileElement>) appLibrary.getCurrentDriverInstance();
	}

	public WelcomeScreen skipToSignIn() {
		AppLibrary.findElementForMobile(driver, skipButton).click();
		return new WelcomeScreen(appLibrary);
	}

	public WelcomeScreen selectLang(String lang) {
		AppLibrary.findElementForMobile(driver, selectLanguageButton).click();
		AppLibrary.verifiyText(driver, languageTitle, "Languages");
		appLibrary.swipeDownByText(lang);
		return new WelcomeScreen(appLibrary);
	}
}
