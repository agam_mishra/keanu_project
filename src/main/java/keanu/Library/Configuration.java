package keanu.Library;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration {

	private String appdir;
	private String appname;
	private String browserName;
	private String deviceId1;
	private String deviceId2;
	private String deviceVersion;
	private String appActivity;
	private String appPackage;
	private Boolean isBrowserStackExecution;
	private String browserStackUserName;
	private String browserStackAuthKey;
	private String browserStackBrowserVersion;
	private String browserStackOS;
	private String browserStackOSVersion1;
	private String browserStackOSVersion2;
	private String browserStackPlatform;
	private String browserStackDevice1;
	private String browserStackDevice2;
	private String isEmulator;
	private String browserVersion;
	private String os;
	private String app_Url;
	private String keanuServer;
	private String user1;
	private String user1Pass;
	private String user2;
	private String user2Pass;
	private String user3;

	public Configuration() {
		final Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(new File("config.properties")));
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		isBrowserStackExecution = (System.getProperty("isBrowserstackExecution") != null
				&& !(System.getProperty("isBrowserstackExecution").equals("${isBrowserstackExecution}")))
						? (System.getProperty("isBrowserstackExecution").equals("true"))
						: Boolean.parseBoolean(prop.getProperty("isbrowserstack.execution", "false").trim());

		browserStackUserName = System.getProperty("browserstack.username") != null
				? System.getProperty("browserstack.username")
				: prop.getProperty("browserstack.username");

		browserStackAuthKey = System.getProperty("browserstack.authkey") != null
				? System.getProperty("browserstack.authkey")
				: prop.getProperty("browserstack.authkey");

		browserStackBrowserVersion = System.getProperty("browserstack.browserversion") != null
				? System.getProperty("browserstack.browserversion")
				: prop.getProperty("browserstack.browserversion");

		browserStackOS = System.getProperty("browserstack.os") != null ? System.getProperty("browserstack.os")
				: prop.getProperty("browserstack.os");

		browserStackOSVersion1 = System.getProperty("browserstack.osversion1") != null
				? System.getProperty("browserstack.osversion1")
				: prop.getProperty("browserstack.osversion1");

		browserStackOSVersion2 = System.getProperty("browserstack.osversion2") != null
				? System.getProperty("browserstack.osversion2")
				: prop.getProperty("browserstack.osversion2");

		browserStackPlatform = System.getProperty("browserstack.platform") != null
				? System.getProperty("browserstack.platform")
				: prop.getProperty("browserstack.platform");

		browserStackDevice1 = System.getProperty("browserstack.devicename1") != null
				? System.getProperty("browserstack.devicename1")
				: prop.getProperty("browserstack.devicename1");

		browserStackDevice2 = System.getProperty("browserstack.devicename2") != null
				? System.getProperty("browserstack.devicename2")
				: prop.getProperty("browserstack.devicename2");

		isEmulator = System.getProperty("browserstack.isEmulator") != null
				? System.getProperty("browserstack.isEmulator")
				: prop.getProperty("browserstack.isEmulator");

		browserVersion = System.getProperty("browser.version") != null ? System.getProperty("browser.version")
				: prop.getProperty("browser.version");

		browserName = System.getProperty("OS.name") != null ? System.getProperty("OS.name")
				: prop.getProperty("OS.name");

		os = System.getProperty("os") != null ? System.getProperty("os") : prop.getProperty("os");

		appdir = System.getProperty("app.dir") != null ? System.getProperty("app.dir") : prop.getProperty("app.dir");

		appname = System.getProperty("app.name") != null ? System.getProperty("app.name")
				: prop.getProperty("app.name");

		deviceId1 = System.getProperty("device.Id1") != null ? System.getProperty("device.Id1")
				: prop.getProperty("device.Id1");

		deviceId2 = System.getProperty("device.Id2") != null ? System.getProperty("device.Id2")
				: prop.getProperty("device.Id2");

		appPackage = System.getProperty("app.package") != null ? System.getProperty("app.package")
				: prop.getProperty("app.package");

		appActivity = System.getProperty("app.activity") != null ? System.getProperty("app.activity")
				: prop.getProperty("app.activity");

		app_Url = System.getProperty("app_Url") != null ? System.getProperty("app_Url") : prop.getProperty("app_Url");

		keanuServer = System.getProperty("keanu.server") != null ? System.getProperty("keanu.server")
				: prop.getProperty("keanu.server");

		user1 = System.getProperty("user1") != null ? System.getProperty("user1") : prop.getProperty("user1");

		user2 = System.getProperty("user2") != null ? System.getProperty("user2") : prop.getProperty("user2");

		user1Pass = System.getProperty("user1.pass") != null ? System.getProperty("user1.pass")
				: prop.getProperty("user1.pass");

		user2Pass = System.getProperty("user2.pass") != null ? System.getProperty("user2.pass")
				: prop.getProperty("user2.pass");

		user3 = System.getProperty("user3") != null ? System.getProperty("user3") : prop.getProperty("user3");
		
	}

	public String getUser3() {
		return user3;
	}

	public String getUser1() {
		return user1;
	}

	public String getUser2() {
		return user2;
	}

	public String getUser1Pass() {
		return user1Pass;
	}

	public String getUser2Pass() {
		return user2Pass;
	}

	public String getKeanuServer() {
		return keanuServer;
	}

	public boolean isBrowserStackExecution() {
		return isBrowserStackExecution;
	}

	public String getBrowserStackUserName() {
		return browserStackUserName;
	}

	public String getBrowserStackAuthKey() {
		return browserStackAuthKey;
	}

	public String getBrowserStackBrowserVersion() {
		return browserStackBrowserVersion;
	}

	public String getBrowserStackOS() {
		return browserStackOS;
	}

	public String getBrowserStackOSVersion1() {
		return browserStackOSVersion1;
	}

	public String getBrowserStackOSVersion2() {
		return browserStackOSVersion2;
	}

	public String getBrowserStackPlatform() {
		return browserStackPlatform;
	}

	public String getBrowserStackDevice1() {
		return browserStackDevice1;
	}

	public String getBrowserStackDevice2() {
		return browserStackDevice2;
	}

	public String getBrowserStackAppURL() {
		return app_Url;
	}

	public String getBrowserStackIsEmulator() {
		return isEmulator;
	}

	public String getOS() {
		return os;
	}

	public String getappDir() {
		return appdir;
	}

	public String getappName() {
		return appname;
	}

	public String getBrowserName() {
		return browserName;
	}

	public String getDeviceId1() {
		return deviceId1;
	}

	public String getDeviceId2() {
		return deviceId2;
	}

	public String getBrowserVersion() {
		return browserVersion;
	}

	public String getAppPackage() {
		return appPackage;
	}

	public String getAppActivity() {
		return appActivity;
	}

	public String getDeviceVersion() {
		return deviceVersion;
	}
}