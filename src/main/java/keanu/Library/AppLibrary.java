package keanu.Library;

import java.io.File;
import java.net.URL;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import keanu.PageObject.AppHeaders;

public class AppLibrary {

	public static final long GLOBALTIMEOUT = 10;
	private AppiumDriver<MobileElement> appiumDriver; // android Driver instance

	private Configuration config;
	public String appDirr;
	public String appName;
	public String deviceId1;
	public String deviceId2;
	public String appPackage;
	public String appActivity;
	public String driverPath;
	public String baseUrl;
	public String mailUrl;
	public String siteName;
	public String browser;
	public String device;
	public boolean isExecutionOnMobile;
	private AppLibrary appLibrary;
	public String currentTestName;
	public String browserStackUserName;
	public String browserStackAuthKey;
	public String browserStackDevice1;
	public String browserStackDevice2;
	public String browserStackOSVersion1;
	public String browserStackOSVersion2;
	public String app_Url;
	Boolean isBrowserStackExecution;

	public String getCurrentTestName() {
		return currentTestName;
	}

	public void setCurrentTestName(String currentTestName) {
		this.currentTestName = currentTestName;
	}

	private String currentSessionID;

	// This is used for parameterized tests
	public AppLibrary(String testName) {
		this.currentTestName = testName;
		this.config = new Configuration();
	}

	public AppLibrary() {
		// do nothign
	}

	/**
	 * Creates an Webdriver Instance @throws Exception
	 */
	public AppiumDriver<MobileElement> getDriverInstance(String mobiledevice, String port) throws Exception {
		DesiredCapabilities caps = new DesiredCapabilities();
		this.config = new Configuration();
		String browser = config.getBrowserName();
		this.browser = browser;

		appDirr = config.getappDir();
		appName = config.getappName();

		deviceId1 = config.getDeviceId1();

		deviceId2 = config.getDeviceId2();

		appPackage = config.getAppPackage();

		appActivity = config.getAppActivity();

		isBrowserStackExecution = config.isBrowserStackExecution();

		System.out.println("Is Browser Stack execution: " + isBrowserStackExecution);

		if (isBrowserStackExecution) {

			try {

				browserStackUserName = config.getBrowserStackUserName();
				System.out.println("browserStackUserName: " + browserStackUserName);

				browserStackAuthKey = config.getBrowserStackAuthKey();
				System.out.println("browserStackAuthKey: " + browserStackAuthKey);

				browserStackOSVersion1 = config.getBrowserStackOSVersion1();
				System.out.println("browserStackOSVersion1: " + browserStackOSVersion1);

				browserStackOSVersion2 = config.getBrowserStackOSVersion2();
				System.out.println("browserStackOSVersion2: " + browserStackOSVersion2);

				browserStackDevice1 = config.getBrowserStackDevice1();
				System.out.println("browserStackDevice1: " + browserStackDevice1);

				browserStackDevice2 = config.getBrowserStackDevice2();
				System.out.println("browserStackDevice2: " + browserStackDevice2);

				app_Url = config.getBrowserStackAppURL();
				System.out.println("app_Url: " + app_Url);

				if (browser.equalsIgnoreCase("android") || browser.equalsIgnoreCase("iPhone")) {

					if (mobiledevice.equals("device1")) {
						caps.setCapability("device", browserStackDevice1);
						caps.setCapability("os_version", browserStackOSVersion1);
					} else {
						caps.setCapability("device", browserStackDevice2);
						caps.setCapability("os_version", browserStackOSVersion2);
					}
					// caps.setCapability("automationName", "uiautomator2");
					caps.setCapability("browserstack.idleTimeout", "180");
					caps.setCapability("browserstack.debug", true);
					caps.setCapability("browserstack.networkLogs", true);
					caps.setCapability("app", app_Url);
					caps.setCapability("newCommandTimeout", 900000);

					appiumDriver = new AppiumDriver<MobileElement>(new URL("https://"
							+ (browserStackUserName.equals("") ? getConfiguration().getBrowserStackUserName()
									: browserStackUserName)
							+ ":" + (browserStackAuthKey.equals("") ? getConfiguration().getBrowserStackAuthKey()
									: browserStackAuthKey)
							+ "@hub-cloud.browserstack.com/wd/hub"), caps);

				}
			} catch (Exception e) {
				Reporter.log("Issue creating new driver instance due to following error: " + e.getMessage() + "\n"
						+ e.getStackTrace(), true);
				throw e;
			}
			currentSessionID = (appiumDriver).getSessionId().toString();
		}

		else if (browser.equalsIgnoreCase("android")) {
			File appDir = new File(appDirr);
			File app = new File(appDir, appName);
			caps.setCapability("device", browser);

			if (mobiledevice.equals("device1")) {
				caps.setCapability("deviceName", deviceId1);
				caps.setCapability("udid", deviceId1);
			} else {
				caps.setCapability("deviceName", deviceId2);
				caps.setCapability("udid", deviceId2);
			}
			// caps.setCapability("automationName", "uiautomator2");
			caps.setCapability("appPackage", appPackage);
			caps.setCapability("appActivity", appActivity);
			caps.setCapability("platformName", browser);
			caps.setCapability("unicodeKeyboard", true);
			caps.setCapability("resetKeyboard", true);
			caps.setCapability("noResetValue", false);
			caps.setCapability("browserstack.appium_version", "1.14.0");
			caps.setCapability("app", app.getAbsolutePath());
			caps.setCapability("autoGrantPermissions", true);
			caps.setCapability("newCommandTimeout", 900000);
			String url = "http://0.0.0.0:" + port + "/wd/hub";

//			String url = "http://192.168.10.131:" + port + "/wd/hub";
			appiumDriver = new AppiumDriver<MobileElement>(new URL(url), caps);
		} else if (browser.equalsIgnoreCase("ios")) {
			File appDir = new File(appDirr);
			File app = new File(appDir, appName);
			caps.setCapability("platformName", "iOS");
			caps.setCapability("platformVersion", "12.2");
			caps.setCapability("deviceName", "iPhone");
			caps.setCapability("automationName", "XCUITest");
			caps.setCapability("xcodeOrgId", "94AWUYGT5Q");
			caps.setCapability("xcodeSigningId", "iPhone Developer");
			caps.setCapability("session-override", "true");
			caps.setCapability("udid", "297437cdc69a6fd12fd3c5ab0721716f48b85a6f");
			caps.setCapability("app", app.getAbsolutePath());
			caps.setCapability("unicodeKeyboard", true);
			caps.setCapability("resetKeyboard", true);
			caps.setCapability("noResetValue", "false");
			caps.setCapability("autoGrantPermissions", true);
			caps.setCapability("newCommandTimeout", 90000);
			appiumDriver = new AppiumDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), caps);
		}
		appiumDriver.manage().timeouts().implicitlyWait(GLOBALTIMEOUT, TimeUnit.SECONDS);
		return appiumDriver;
	}

	/**
	 * Creates an Webdriver Instance @throws Exception
	 */
	public AppiumDriver<MobileElement> getDriverInstance() throws Exception {
		DesiredCapabilities caps = new DesiredCapabilities();
		String browser = config.getBrowserName();
		this.browser = browser;

		appDirr = config.getappDir();
		appName = config.getappName();

		deviceId1 = config.getDeviceId1();

		appPackage = config.getAppPackage();

		appActivity = config.getAppActivity();
		isBrowserStackExecution = config.isBrowserStackExecution();
		System.out.println("Is Browser Stack execution: " + isBrowserStackExecution);

		if (isBrowserStackExecution) {

			try {

				browserStackUserName = config.getBrowserStackUserName();
				System.out.println("browserStackUserName: " + browserStackUserName);
				
				browserStackAuthKey = config.getBrowserStackAuthKey();
				System.out.println("browserStackAuthKey: " + browserStackAuthKey);

				browserStackOSVersion1 = config.getBrowserStackOSVersion1();
				System.out.println("browserStackOSVersion1: " + browserStackOSVersion1);

				browserStackDevice1 = config.getBrowserStackDevice1();
				System.out.println("browserStackDevice1: " + browserStackDevice1);

				app_Url = config.getBrowserStackAppURL();
				System.out.println("app_Url: " + app_Url);
				

				if (browser.equalsIgnoreCase("android") || browser.equalsIgnoreCase("iPhone")) {

					// caps.setCapability("automationName", "uiautomator2");
					caps.setCapability("browserstack.idleTimeout", "180");
					caps.setCapability("device", browserStackDevice1);
					caps.setCapability("os_version", browserStackOSVersion1);
					caps.setCapability("browserstack.debug", true);
					caps.setCapability("browserstack.networkLogs", true);
					caps.setCapability("browserstack.appium_version", "1.14.0");
					caps.setCapability("app", app_Url);
					caps.setCapability("newCommandTimeout", 900000);
					caps.setCapability("build", System.getProperty("Build"));
					caps.setCapability("project", System.getProperty("Suite"));
					caps.setCapability("name", currentTestName);

					appiumDriver = new AppiumDriver<MobileElement>(new URL("https://"
							+ (browserStackUserName.equals("") ? getConfiguration().getBrowserStackUserName()
									: browserStackUserName)
							+ ":" + (browserStackAuthKey.equals("") ? getConfiguration().getBrowserStackAuthKey()
									: browserStackAuthKey)
							+ "@hub-cloud.browserstack.com/wd/hub"), caps);
				}
			} catch (Exception e) {
				Reporter.log("Issue creating new driver instance due to following error: " + e.getMessage() + "\n"
						+ e.getStackTrace(), true);
				throw e;
			}
			currentSessionID = (appiumDriver).getSessionId().toString();
		}

		else if (browser.equalsIgnoreCase("android")) {
			File appDir = new File(appDirr);
			File app = new File(appDir, appName);
			caps.setCapability("device", browser);
			caps.setCapability("deviceName", deviceId1);
			caps.setCapability("udid", deviceId1);
			caps.setCapability("appPackage", appPackage);
			caps.setCapability("appActivity", appActivity);
			caps.setCapability("platformName", browser);
			caps.setCapability("unicodeKeyboard", true);
			caps.setCapability("resetKeyboard", true);
			caps.setCapability("noResetValue", false);
			caps.setCapability("app", app.getAbsolutePath());
			caps.setCapability("autoGrantPermissions", true);
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 90000);
//			caps.setCapability("newCommandTimeout", 20000);
			String url = "http://0.0.0.0:4723/wd/hub";
//			String url = "http://192.168.10.131:4723/wd/hub";
			appiumDriver = new AppiumDriver<MobileElement>(new URL(url), caps);

		} else if (browser.equalsIgnoreCase("ios")) {
			File appDir = new File(appDirr);
			File app = new File(appDir, appName);
			caps.setCapability("platformName", "iOS");
			caps.setCapability("platformVersion", "12.2");
			caps.setCapability("deviceName", "iPhone");
			caps.setCapability("automationName", "XCUITest");
			caps.setCapability("xcodeOrgId", "94AWUYGT5Q");
			caps.setCapability("xcodeSigningId", "iPhone Developer");
			caps.setCapability("session-override", "true");
			caps.setCapability("udid", "297437cdc69a6fd12fd3c5ab0721716f48b85a6f");
			caps.setCapability("app", app.getAbsolutePath());
			caps.setCapability("unicodeKeyboard", true);
			caps.setCapability("resetKeyboard", true);
			caps.setCapability("noResetValue", "false");
			caps.setCapability("autoGrantPermissions", true);
			caps.setCapability("newCommandTimeout", 20000);
			appiumDriver = new AppiumDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), caps);
		}
		appiumDriver.manage().timeouts().implicitlyWait(GLOBALTIMEOUT, TimeUnit.SECONDS);
		return appiumDriver;
	}

	public void reopenApp() throws Exception {
		try {
			try {
				AppLibrary.sleep(10000);
				AppLibrary.findElementForMobile(appiumDriver, "id:android:id/aerr_close").click();
				AppLibrary.sleep(3000);
				if (isBrowserStackExecution)
					appiumDriver.launchApp();
				else
					appiumDriver.activateApp(appPackage);
			} catch (Exception e) {
				// AppLibrary.findElementForMobile(appiumDriver,
				// AppHeaders.myProfileScreen);
				// AppLibrary.sleep(1000);
				AppLibrary.findElementForMobile(appiumDriver, AppHeaders.myProfileScreen).click();
				AppLibrary.sleep(10000);
				AppLibrary.findElementForMobile(appiumDriver, "id:android:id/aerr_close").click();
				AppLibrary.sleep(3000);
				if (isBrowserStackExecution)
					appiumDriver.launchApp();
				else
					appiumDriver.activateApp(appPackage);
			}
			AppLibrary.sleep(3000);
		} catch (Exception e) {
			AppLibrary.sleep(3000);
			if (isBrowserStackExecution)
				appiumDriver.launchApp();
			else
				appiumDriver.activateApp(appPackage);
			AppLibrary.sleep(3000);
			try {
				AppLibrary.findElementForMobile(appiumDriver, AppHeaders.myProfileScreen).click();
				AppLibrary.sleep(10000);
				AppLibrary.findElementForMobile(appiumDriver, "id:android:id/aerr_close").click();
				AppLibrary.sleep(3000);
				if (isBrowserStackExecution)
					appiumDriver.launchApp();
				else
					appiumDriver.activateApp(appPackage);
				AppLibrary.findElementForMobile(appiumDriver, AppHeaders.myProfileScreen);
			} catch (Exception e1) {
				AppLibrary.sleep(10000);
				AppLibrary.findElementForMobile(appiumDriver, "id:android:id/aerr_close").click();
				AppLibrary.sleep(3000);
				if (isBrowserStackExecution)
					appiumDriver.launchApp();
				else
					appiumDriver.activateApp(appPackage);

				AppLibrary.sleep(3000);
				AppLibrary.findElementForMobile(appiumDriver, AppHeaders.myProfileScreen);
			}
		}
	}

	public void openApp() throws Exception {
		AppLibrary.sleep(3000);
		if (isBrowserStackExecution) {
			appiumDriver.activateApp(appPackage);
			appiumDriver.rotate(ScreenOrientation.PORTRAIT);
		} else
			appiumDriver.activateApp(appPackage);
		AppLibrary.sleep(5000);
	}

	public void terminateApp() throws Exception {
		appiumDriver.terminateApp(appPackage);
		appiumDriver.activateApp(appPackage);
	}

	public static int randIntDigits(int min, int max) {
		Random rand = new Random();
		int randomNum = (rand.nextInt((max - min) + 1) + rand.nextInt((max - min) + 1)) / 2;
		return randomNum;
	}

	/**
	 * Get Driver Instance
	 */
	public AppiumDriver<MobileElement> getCurrentDriverInstance() {
		return appiumDriver;
	}

	/**
	 * Closes the Browser
	 */
	public void closeBrowser() {
		if (appiumDriver != null)
			appiumDriver.quit();
	}

	public static MobileElement findElementForMobile(AppiumDriver<MobileElement> driver, String locatorString) {
		String string = locatorString;
		String[] parts = string.split(":");
		String type = parts[0]; // 004
		String object = parts[1];
		if (parts.length > 2) {
			for (int i = 2; i < parts.length; i++) {
				object = object + ":" + parts[i];
			}
		}
		Reporter.log("Finding element with logic: " + locatorString, true);
		System.out.println("Finding element with logic: " + locatorString);
		MobileElement element = null;
		if (type.equals("id")) {
			element = driver.findElement(By.id(object));
		} else if (type.equals("xpath")) {
			element = driver.findElement(By.xpath(object));
		} else if (type.equals("name")) {
			element = driver.findElement(By.name(object));
		} else if (type.equals("class")) {
			element = driver.findElement(By.className(object));
		} else if (type.equals("link")) {
			element = driver.findElement(By.linkText(object));
		} else if (type.equals("partiallink")) {
			element = driver.findElement(By.partialLinkText(object));
		} else if (type.equals("css")) {
			element = driver.findElement(By.cssSelector(object));
		} else {
			throw new RuntimeException("Please provide correct element locating strategy");
		}
		return element;
	}

	public static WebElement findElementForMobile(AppiumDriver<MobileElement> driver, String locatorString,
			boolean isOptional) {
		try {
			System.out.println(locatorString);
			return findElementForMobile(driver, locatorString);
		} catch (NoSuchElementException nse) {
			if (isOptional) {
				return null;
			} else {
				throw new RuntimeException("Element " + locatorString + " not found");
			}
		}
	}

	public Configuration getConfiguration() {
		if (config == null) {
			config = new Configuration();
		}
		return config;
	}

	public static boolean verifyMobileElement(AppiumDriver<MobileElement> driver, String locatorString) {
		return verifyMobileElement(driver, locatorString, false);
	}

	public static boolean verifyMobileElement(AppiumDriver<MobileElement> driver, String locatorString,
			boolean checkVisibility) {
		boolean isDisplayed = true;
		try {
			if (checkVisibility) {
				isDisplayed = (findElementForMobile(driver, locatorString).isDisplayed());
			} else {
				findElementForMobile(driver, locatorString);
			}
		} catch (NoSuchElementException nsee) {
			Assert.assertTrue(false, "Element not found using locator: " + locatorString);
		}
		return isDisplayed;
	}

	public static void sleep(long milliSeconds) {
		try {
			Thread.sleep(milliSeconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static List<MobileElement> findElementsForMobile(AppiumDriver<MobileElement> driver, String locatorString) {

		String string = locatorString;
		String[] parts = string.split(":");
		String type = parts[0]; // 004
		String object = parts[1];
		if (parts.length > 2) {
			for (int i = 2; i < parts.length; i++) {
				object = object + ":" + parts[i];
			}
		}

		Reporter.log("Finding element with logic: " + locatorString, true);
		List<MobileElement> element = null;
		if (type.equals("id")) {
			element = driver.findElements(By.id(object));
		} else if (type.equals("xpath")) {
			element = driver.findElements(By.xpath(object));
		} else if (type.equals("name")) {
			element = driver.findElements(By.name(object));
		} else if (type.equals("class")) {
			element = driver.findElements(By.className(object));
		} else if (type.equals("link")) {
			element = driver.findElements(By.linkText(object));
		} else if (type.equals("partiallink")) {
			element = driver.findElements(By.partialLinkText(object));
		} else if (type.equals("css")) {
			element = driver.findElements(By.cssSelector(object));
		} else {
			throw new RuntimeException("Please provide correct element locating strategy");
		}
		return element;
	}

	public String getCurrentSessionID() {
		return currentSessionID;
	}

	public static void waitForMobileElementClickable(AppiumDriver<MobileElement> driver, WebElement element) {
		new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(element));
	}

	public static void waitForMobileElementVisible(AppiumDriver<MobileElement> driver, WebElement element) {
		new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(element));
	}

	public void selectCheckBox(AppiumDriver<MobileElement> driver, String locator) throws Exception {
		if (!AppLibrary.findElementForMobile(driver, locator).isSelected()) {
			AppLibrary.clickMobileElement(driver, locator);
		}
	}

	public static void deselectCheckBox(AppiumDriver<MobileElement> driver, String locator) throws Exception {
		if (AppLibrary.findElementForMobile(driver, locator).isSelected()) {
			AppLibrary.clickMobileElement(driver, locator);
		}
	}

	public static void enterMobileText(AppiumDriver<MobileElement> driver, String locator, String text) {
		AppLibrary.findElementForMobile(driver, locator).click();
		AppLibrary.findElementForMobile(driver, locator).clear();
		AppLibrary.findElementForMobile(driver, locator).sendKeys(text);
	}

	public static void clickMobileElement(AppiumDriver<MobileElement> driver, String locator) throws Exception {
		AppLibrary.findElementForMobile(driver, locator).click();
	}

	public static boolean verifyCheckBox(AppiumDriver<MobileElement> driver, String locator) {
		return AppLibrary.findElementForMobile(driver, locator).isSelected();
	}

	public static void waitUntilMobileElementDisplayed(AppiumDriver<MobileElement> driver, String locatorString) {

		WebDriverWait wait = new WebDriverWait(driver, 30);
		String string = locatorString;
		String[] parts = string.split(":");
		String type = parts[0];
		String object = parts[1];

		Reporter.log("Finding element with logic: " + locatorString, true);
		if (type.equals("id")) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(object)));
		} else if (type.equals("name")) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(object)));
		} else if (type.equals("class")) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(object)));
		} else if (type.equals("link")) {
			;
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(object)));
		} else if (type.equals("partiallink")) {
			;
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(object)));
		} else if (type.equals("css")) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(object)));
		} else if (type.equals("xpath")) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(object)));
		} else {
			throw new RuntimeException("Please provide correct element locating strategy");
		}
	}

	public static void verifyAbsent(AppiumDriver<MobileElement> driver, String locatorString) {

		String string = locatorString;
		String[] parts = string.split(":");
		String type = parts[0]; // 004
		String object = parts[1];

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement element = null;
		try {
			Reporter.log("Finding element with logic: " + locatorString, true);
			if (type.equals("id")) {
				element = driver.findElement(By.id(object));
			} else if (type.equals("name")) {
				element = driver.findElement(By.name(object));
			} else if (type.equals("class")) {
				element = driver.findElement(By.className(object));
			} else if (type.equals("link")) {
				element = driver.findElement(By.linkText(object));
			} else if (type.equals("partiallink")) {
				element = driver.findElement(By.partialLinkText(object));
			} else if (type.equals("css")) {
				element = driver.findElement(By.cssSelector(object));
			} else if (type.equals("xpath")) {
				element = driver.findElement(By.xpath(object));
			}
			Assert.assertTrue(false,
					"Expected element to be absent, but it was found on the page. Text = " + element.getText());

		} catch (Exception e) {
			// It's good if not found
		} finally {
			driver.manage().timeouts().implicitlyWait(AppLibrary.GLOBALTIMEOUT, TimeUnit.SECONDS);
		}
	}

	public static void verifiyText(AppiumDriver<MobileElement> driver, String locator, String expectedValue) {
		String actualValue = AppLibrary.findElementForMobile(driver, locator).getText();
		Assert.assertEquals(actualValue, expectedValue,
				"values didnot match for " + locator + "Expected =" + expectedValue + "  Actual =" + actualValue);
	}

	public static void verifiyContains(AppiumDriver<MobileElement> driver, String locator, String expectedValue) {
		String actualValue = AppLibrary.findElementForMobile(driver, locator).getText();
		actualValue.contains(expectedValue);
	}

	public void swipeDownByText(String text) {
		appiumDriver.findElement(MobileBy.AndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""
						+ text + "\").instance(0))"))
				.click();
	}

	public void horizontalSwipe(String startLocator, String endLocator) {
		MobileElement startElement = AppLibrary.findElementForMobile(appiumDriver, startLocator);
		MobileElement endElement = AppLibrary.findElementForMobile(appiumDriver, endLocator);

		int startX = startElement.getLocation().getX() + (startElement.getSize().getWidth() / 2);
		int startY = startElement.getLocation().getY() + (startElement.getSize().getHeight() / 2);

		int endX = endElement.getLocation().getX() + (endElement.getSize().getWidth() / 2);
		int endY = endElement.getLocation().getY() + (endElement.getSize().getHeight() / 2);
		TouchAction touchAction = new TouchAction(appiumDriver);

		touchAction.longPress(PointOption.point(startX, startY)).moveTo(PointOption.point(endX, endY)).release()
				.perform();
		AppLibrary.sleep(1000);
	}

	public void horizontalRightSwipe(String startLocator) {
		WebElement element = AppLibrary.findElementForMobile(appiumDriver, startLocator);
		Dimension dim = element.getSize();
		int Ancor = element.getSize().getHeight() / 2;

		Double screenWidthStart = dim.getWidth() * 0.8;
		int scrollStart = screenWidthStart.intValue();

		Double screenWidthEnd = dim.getWidth() * 0.2;
		int scrollEnd = screenWidthEnd.intValue();

		AndroidTouchAction touch = new AndroidTouchAction((PerformsTouchActions) appiumDriver);
		touch.press(PointOption.point(scrollStart, Ancor)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(300)))
				.moveTo(PointOption.point(scrollEnd, Ancor)).release().perform();

//		AndroidTouchAction touch = new AndroidTouchAction((PerformsTouchActions) appiumDriver);
//		touch.longPress(PointOption.point(scrollStart, Ancor)).

		AppLibrary.sleep(1000);

	}

	public void horizontalLeftSwipe(String startLocator) {
		WebElement element = AppLibrary.findElementForMobile(appiumDriver, startLocator);
		Dimension dim = element.getSize();
		int Ancor = element.getSize().getHeight() / 2;

		Double screenWidthStart = dim.getWidth() * 0.8;
		int scrollStart = screenWidthStart.intValue();

		Double screenWidthEnd = dim.getWidth() * 0.2;
		int scrollEnd = screenWidthEnd.intValue();

		AndroidTouchAction touch = new AndroidTouchAction((PerformsTouchActions) appiumDriver);
		touch.press(PointOption.point(scrollEnd, Ancor)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(300)))
				.moveTo(PointOption.point(scrollStart, Ancor)).release().perform();
		AppLibrary.sleep(1000);
	}
}